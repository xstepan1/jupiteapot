#pragma once

#include "light.hpp"

#include <program.hpp>
#include <scene.hpp>

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <memory>
#include <vector>

class Probe {
public:
	Probe(const std::string& objPath,
	    const glm::vec3& startPosition,
	    float speed,
	    std::vector<LightUBO>& lights,
	    Program& texturedObjectProgram,
	    Program& normalMappedObjectProgram);

public:
	void update();

	void draw();

	glm::mat4& modelMatrix() {
		return _modelMatrix;
	}

	float speed() const {
		return _speed;
	}

	double deltaAngle() const {
		return _deltaAngle;
	}

	const glm::vec3& position() const {
		return _position;
	}

private:
	std::unique_ptr<Scene> _scene;
	float _speed = 0.0f;
	glm::mat4 _modelMatrix;
	glm::vec3 _startPosition;
	Program& _texturedObjectProgram;
	Program& _normalMappedObjectProgram;
	double _time = 0.0;
	double _lastTime = 0.0;
	double _deltaAngle = 0.0;
	glm::vec3 _position = glm::vec3(0.0f);
	std::vector<LightUBO>& _lights;
	std::vector<glm::vec3> _lightsStartPositions;
	std::size_t _lightsStart = 0;
	std::size_t _lightsCount = 0;
};
