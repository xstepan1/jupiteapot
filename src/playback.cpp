#include "playback.hpp"
#define DR_FLAC_IMPLEMENTATION
#include <dr_flac.h>
#define MINIAUDIO_IMPLEMENTATION
#include <miniaudio.h>
#include <stdexcept>
#include <fft.hpp>
#include <cstring>

void dataCallback(ma_device* pDevice, void* pOutput, const void* pInput, ma_uint32 frameCount) {
	Playback* playback = (Playback*)pDevice->pUserData;
	if (playback == nullptr) {
		return;
	}
	ma_uint64 readCount = ma_decoder_read_pcm_frames(playback->_decoder.get(), pOutput, frameCount);
	double input[Playback::magicCount * 2];
	double realOutput[Playback::magicCount * 2];
	double imaginaryOutput[Playback::magicCount * 2];
	if (readCount >= Playback::magicCount * 2) {
		for (size_t i = 0; i < Playback::magicCount * 2; i++) {
			input[i] = ((double)((std::int32_t*)pOutput)[i]) / 2147483648.0;
		}
		fft(input, nullptr, Playback::magicCount * 2, realOutput, imaginaryOutput);
		for (size_t i = 0; i < Playback::magicCount; i++) {
			playback->magic[i] = sqrt(sqrt(realOutput[i] * realOutput[i] + imaginaryOutput[i] * imaginaryOutput[i]));
		}
	}
	(void)pInput;
}

Playback::Playback(const std::string& flacFile) {
	if (ma_decoder_init_file(flacFile.c_str(), nullptr, _decoder.get()) != MA_SUCCESS) {
		throw std::logic_error("File could not be played.");
	}

	_config = ma_device_config_init(ma_device_type_playback);
	_config.playback.format = _decoder->outputFormat;
	_config.playback.channels = _decoder->outputChannels;
	_config.sampleRate = _decoder->outputSampleRate;
	_config.dataCallback = dataCallback;
	_config.pUserData = this;

	if (ma_device_init(nullptr, &_config, _device.get()) != MA_SUCCESS) {
		ma_decoder_uninit(_decoder.get());
		throw std::logic_error("Failed to initialize a playback device.");
	}

	if (ma_device_start(_device.get()) != MA_SUCCESS) {
		ma_device_uninit(_device.get());
		ma_decoder_uninit(_decoder.get());
		throw std::logic_error("Failed to start the playback device.");
	}
}

Playback::~Playback() {
	ma_device_uninit(_device.get());
	ma_decoder_uninit(_decoder.get());
}
