#pragma once

#include <memory>
#include <mesh.hpp>
#include <program.hpp>
#include <texture.hpp>

class Skybox {
public:
	Skybox(const std::string& prefix);

	void draw();

	Texture& cubemap() {
		return _cubemap;
	}

	Program& program() {
		return _skyboxProgram;
	}

private:
	static const float vertices[108];

	Texture _cubemap;
	Program _skyboxProgram = Program("shaders/skybox.vert", "shaders/skybox.frag");
	Mesh _cube;
};
