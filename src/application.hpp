#pragma once

#include "mesh.hpp"
#include "playback.hpp"
#include "probe.hpp"
#include "program.hpp"
#include "rotate_camera.hpp"
#include "scene.hpp"
#include "skybox.hpp"
#include "texture.hpp"
#include "utilities.hpp"
#include "light.hpp"
#include <GLFW/glfw3.h>
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <memory>
#include <vector>

// Used for clearing custom framebuffers (glClearNamedFramebufferfv)
const float clearColor[4] = { 0.0, 0.0, 0.0, 1.0 };
const float clearDepth[1] = { 1.0 };

struct CameraUBO {
	glm::mat4 projection;
	glm::mat4 view;
	glm::vec4 position;
};

struct ToonUBO {
	GLint levels;
	GLuint isEnabled;
	GLint64 hack;
};

class Application {
public:
	Application(size_t initialWidth, size_t initialHeight);
	~Application();

	void render();

	void onResize(GLFWwindow *window, int width, int height);
	void onMouseMove(GLFWwindow *window, double x, double y);
	void onMousePressed(GLFWwindow *window, int button, int action, int mods);
	void onKeyPressed(GLFWwindow *window, int key, int scancode, int action, int mods);

private:
	size_t width;
	size_t height;

	RotateCamera camera;
	CameraUBO cameraUbo;
	ToonUBO toonUbo;
	GLuint cameraBuffer = 0;
	GLuint toonBuffer = 0;
	GLuint beltBuffer = 0;

	Program jupiterProgram = Program("shaders/jupiter.vert", "shaders/jupiter.frag");
	Program beltProgram = Program("shaders/belt.vert", "shaders/belt.frag");
	Program drawLightsProgram = Program("shaders/draw_lights.vert", "shaders/draw_lights.frag");
	Program texturedObjectProgram = Program("shaders/texturedObject.vert", "shaders/texturedObject.frag");
	Program normalMappedObjectProgram = Program("shaders/normalMappedObject.vert", "shaders/normalMappedObject.frag");

	Mesh cube = Mesh::cube();
	Mesh teapot = Mesh::teapot();
	Mesh polarTeapot = Mesh::polarTeapot();

	std::unique_ptr<Probe> pioneer;
	std::unique_ptr<Probe> voyager;
	std::unique_ptr<Probe> ulysses;
	std::unique_ptr<Probe> galileo;
	std::unique_ptr<Probe> cassini;
	std::unique_ptr<Probe> juno;

	Material jupiterMaterial;
	glm::mat4 jupiterModelMatrix;
	Material beltMaterial;
	glm::mat4 beltModelMatrix;

	Skybox skybox = Skybox("skybox");

	Texture jupiterTexture = Texture("images/jupiter.png");
	Texture ringsTexture = Texture("images/rings.png");

	std::vector<LightUBO> lights;
	GLuint lightsBuffer = 0;

	GLuint postprocessFramebuffer = 0;
	GLuint postprocessFramebufferColor = 0;
	GLuint postprocessFramebufferDepth = 0;

	std::unique_ptr<Playback> playback;

	double time = 0.0f;
	double lastTime = 0.0f;
	std::vector<double> music = std::vector<double>(Playback::magicCount);
	const double musicStepDown = -0.03;
	Probe *followedProbe = nullptr;
};