#include "probe.hpp"

Probe::Probe(const std::string& objPath,
    const glm::vec3& startPosition,
    float speed,
    std::vector<LightUBO>& lights,
    Program& texturedObjectProgram,
    Program& normalMappedObjectProgram)
    : _startPosition(startPosition)
    , _speed(speed)
    , _modelMatrix(glm::translate(glm::mat4(1.0f), startPosition))
    , _lights(lights)
    , _lightsStart(lights.size())
    , _texturedObjectProgram(texturedObjectProgram)
    , _normalMappedObjectProgram(normalMappedObjectProgram) {

	_scene = std::make_unique<Scene>(objPath);
	for (auto& mesh : _scene->meshes()) {
		if (mesh->material() && glm::vec3(mesh->material()->emissiveColor()) != glm::vec3(0.0f)) {
			lights.push_back(LightUBO { glm::vec4(mesh->center(), 1.0f),
			    glm::vec4(0.0f),
			    mesh->material()->emissiveColor(),
			    glm::vec4(0.0f) });
			++_lightsCount;
			_lightsStartPositions.push_back(mesh->center());
		}
	}
}

void Probe::update() {
	_time = glfwGetTime();
	if (_lastTime > 0.0) {
		_deltaAngle = (_time - _lastTime) * _speed;
		_modelMatrix = glm::rotate(glm::mat4(1.0f), float(_deltaAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * _modelMatrix;
		_position = _modelMatrix * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
		for (size_t i = _lightsStart; i < _lightsStart + _lightsCount; i++) {
			_lights[i].position = _modelMatrix * glm::vec4(_lightsStartPositions[i - _lightsStart], 1.0f);
		}
	}
	_lastTime = _time;
}

void Probe::draw() {
	for (auto& mesh : _scene->meshes()) {
		GLuint program = mesh->material() && mesh->material()->normalTexture()
		    ? _normalMappedObjectProgram.id()
		    : _texturedObjectProgram.id();
		glUseProgram(program);
		glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, glm::value_ptr(_modelMatrix));
		mesh->draw();
	}
}