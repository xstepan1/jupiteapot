#pragma once

#include <glm/glm.hpp>

struct LightUBO {
	glm::vec4 position;
	glm::vec4 ambientColor;
	glm::vec4 diffuseColor;
	glm::vec4 specularColor;
};
