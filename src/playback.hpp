#pragma once

#include <miniaudio.h>
#include <string>
#include <vector>
#include <memory>

void dataCallback(ma_device* pDevice, void* pOutput, const void* pInput, ma_uint32 frameCount);

class Playback {
public:
	static const std::size_t magicCount = 64;

	std::vector<double> magic = std::vector<double>(magicCount);

	Playback(const std::string& flacPath);
	~Playback();

private:
	std::unique_ptr<ma_decoder> _decoder = std::make_unique<ma_decoder>();
	std::unique_ptr<ma_device> _device = std::make_unique<ma_device>();
	ma_device_config _config;

	friend void dataCallback(ma_device* pDevice, void* pOutput, const void* pInput, ma_uint32 frameCount);
};
