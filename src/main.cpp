#include "application.hpp"
#include <GLFW/glfw3.h>
#include <glad/glad.h>
#include <iostream>

#define SUCCESS 0
#define ERR_GLFW_INIT 1
#define ERR_GLFW_OTHER 2
#define ERR_GLAD_LOAD 3

const size_t INITIAL_WIDTH = 1280;
const size_t INITIAL_HEIGHT = 720;

void onGlfwError(int errorCode, const char *description);
void APIENTRY onGlDebugMessage(GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const char *message,
    const void *user_parameter);
void onResize(GLFWwindow *window, int width, int height);
void onMouseMove(GLFWwindow *window, double x, double y);
void onMousePressed(GLFWwindow *window, int button, int action, int mods);
void onKeyPressed(GLFWwindow *window, int key, int scancode, int action, int mods);

int main(void) {
	// GLFW
	if (!glfwInit()) {
		std::cerr << "Could not initialize GLFW!" << std::endl;
		return ERR_GLFW_INIT;
	}
	glfwSetErrorCallback(onGlfwError);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);

	//glfwWindowHint(GLFW_SAMPLES, 4);
	    GLFWwindow *window
	    = glfwCreateWindow(INITIAL_WIDTH, INITIAL_HEIGHT, "The Almighty JupiTeapot", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return ERR_GLFW_OTHER;
	}
	glfwMakeContextCurrent(window);

	// GLAD
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cerr << "Could not initialize OpenGL context!" << std::endl;
		return ERR_GLAD_LOAD;
	}
	//glEnable(GL_MULTISAMPLE);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(onGlDebugMessage, nullptr);

	{
		Application application(INITIAL_WIDTH, INITIAL_HEIGHT);
		glfwSetWindowUserPointer(window, &application);
		glfwSetWindowSizeCallback(window, onResize);
		glfwSetCursorPosCallback(window, onMouseMove);
		glfwSetMouseButtonCallback(window, onMousePressed);
		glfwSetKeyCallback(window, onKeyPressed);

		while (!glfwWindowShouldClose(window)) {
			application.render();
			glfwSwapBuffers(window);
			glfwPollEvents();
		}

		// Free the application before termitating GLFW
	}

	glfwTerminate();
	return SUCCESS;
}

void onGlfwError(int errorCode, const char *description) {
	std::cerr << "\033[31m[GLFW ERROR]\033[37m " << description << std::endl;
}

void APIENTRY onGlDebugMessage(GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const char *message,
    const void *user_parameter) {
	const char *tag;
	switch (type) {
	case GL_DEBUG_TYPE_ERROR:
		tag = "\033[31m[GL ERROR]\033[37m ";
		throw std::logic_error(message);
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		tag = "\033[31m[GL DEPRECATED BEHAVIOR]\033[37m ";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		tag = "\033[31m[GL UNDEFINED BEHAVIOR]\033[37m ";
		break;
	default:
		tag = "\033[1;30m[GL INFO]\033[37m ";
		break;
	}

	std::cerr << tag << message << std::endl;
}

void onResize(GLFWwindow *window, int width, int height) {
	Application *application = (Application *)glfwGetWindowUserPointer(window);
	application->onResize(window, width, height);
}

void onMouseMove(GLFWwindow *window, double x, double y) {
	Application *application = (Application *)glfwGetWindowUserPointer(window);
	application->onMouseMove(window, x, y);
}

void onMousePressed(GLFWwindow *window, int button, int action, int mods) {
	Application *application = (Application *)glfwGetWindowUserPointer(window);
	application->onMousePressed(window, button, action, mods);
}

void onKeyPressed(GLFWwindow *window, int key, int scancode, int action, int mods) {
	Application *application = (Application *)glfwGetWindowUserPointer(window);
	application->onKeyPressed(window, key, scancode, action, mods);
}
