#include "application.hpp"

Application::Application(size_t initialWidth, size_t initialHeight)
    : width(initialWidth)
    , height(initialHeight) {

	pioneer = std::make_unique<Probe>(Probe("objects/pioneer/pioneer.obj", { 0.0f, 4.0f, 15.0f }, 0.2f, lights, texturedObjectProgram, normalMappedObjectProgram));
	voyager = std::make_unique<Probe>(Probe("objects/voyager/voyager.obj", { 20.0f, 2.0f, 0.0f }, -0.1f, lights, texturedObjectProgram, normalMappedObjectProgram));
	ulysses = std::make_unique<Probe>(Probe("objects/ulysses/ulysses.obj", { 20.0f, -3.0f, 20.0f }, 0.15f, lights, texturedObjectProgram, normalMappedObjectProgram));
	galileo = std::make_unique<Probe>(Probe("objects/galileo/galileo.obj", { 8.0f, 6.0f, 8.0f }, -0.4f, lights, texturedObjectProgram, normalMappedObjectProgram));
	cassini = std::make_unique<Probe>(Probe("objects/cassini/cassini.obj", { -10.0f, -4.0f, -10.0f }, -0.2f, lights, texturedObjectProgram, normalMappedObjectProgram));
	juno = std::make_unique<Probe>(Probe("objects/juno/Juno.obj", { 25.0f, 0.5f, 25.0f }, -0.05f, lights, texturedObjectProgram, normalMappedObjectProgram));

	cameraUbo.position = glm::vec4(camera.eyePosition(), 1.0f);
	cameraUbo.projection = glm::perspective(glm::radians(45.0f), float(width) / float(height), 0.01f, 1000.0f);
	cameraUbo.view = glm::lookAt(camera.eyePosition(), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));

	LightUBO directionalLight;
	directionalLight.position = glm::vec4(1.0f, 1.0f, 1.0f, 0.0f);
	directionalLight.ambientColor = glm::vec4(0.0f);
	directionalLight.diffuseColor = glm::vec4(1.0f);
	directionalLight.specularColor = glm::vec4(1.0f);
	lights.push_back(directionalLight);

	toonUbo.levels = 8;
	toonUbo.isEnabled = false;

	jupiterModelMatrix = glm::scale(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -5.0f, 0.0f)), glm::vec3(7.0f));
	jupiterMaterial.ambientColor(glm::vec4(0.0f));
	jupiterMaterial.diffuseColor(glm::vec4(1.0f));
	jupiterMaterial.specularColor(glm::vec4(1.0f, 1.0f, 1.0f, 64.0f));

	beltModelMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(0.1f));
	beltMaterial.ambientColor(glm::vec4(0.0f));
	beltMaterial.diffuseColor(glm::vec4(1.0f));
	beltMaterial.specularColor(glm::vec4(1.0f, 1.0f, 1.0f, 2048.0f));

	pioneer->modelMatrix() = glm::scale(pioneer->modelMatrix(), glm::vec3(0.2f));
	pioneer->modelMatrix() = glm::rotate(pioneer->modelMatrix(), -3.14159f / 8.0f, glm::vec3(1.0, 0.0, 0.0));

	voyager->modelMatrix() = glm::scale(voyager->modelMatrix(), glm::vec3(0.1f));
	voyager->modelMatrix() = glm::rotate(voyager->modelMatrix(), 3.14159f / 2.0f, glm::vec3(1.0, 0.0, 0.0));
	voyager->modelMatrix() = glm::rotate(voyager->modelMatrix(), 3.14159f / 2.0f, glm::vec3(0.0, 0.0, 1.0));

	ulysses->modelMatrix() = glm::scale(ulysses->modelMatrix(), glm::vec3(0.5f));
	ulysses->modelMatrix() = glm::rotate(ulysses->modelMatrix(), 3.14159f / 4.0f, glm::vec3(0.0, 1.0, 0.0));
	ulysses->modelMatrix() = glm::rotate(ulysses->modelMatrix(), -3.14159f / 2.5f, glm::vec3(1.0, 0.0, 0.0));

	galileo->modelMatrix() = glm::scale(galileo->modelMatrix(), glm::vec3(0.1f));
	galileo->modelMatrix() = glm::rotate(galileo->modelMatrix(), 3.14159f / 16.0f, glm::vec3(1.0, 0.0, 0.0));
	galileo->modelMatrix() = glm::rotate(galileo->modelMatrix(), -3.14159f / 16.0f, glm::vec3(0.0, 0.0, 1.0));

	cassini->modelMatrix() = glm::scale(cassini->modelMatrix(), glm::vec3(0.2f));

	juno->modelMatrix() = glm::scale(juno->modelMatrix(), glm::vec3(0.1f));
	juno->modelMatrix() = glm::rotate(juno->modelMatrix(), -3.14159f / 4.0f, glm::vec3(0.0, 1.0, 0.0));
	juno->modelMatrix() = glm::rotate(juno->modelMatrix(), 3.14159f / 2.0f, glm::vec3(1.0, 0.0, 0.0));

	glCreateBuffers(1, &cameraBuffer);
	glNamedBufferStorage(cameraBuffer, sizeof(CameraUBO), &cameraUbo, GL_DYNAMIC_STORAGE_BIT);

	glCreateBuffers(1, &lightsBuffer);
	glNamedBufferStorage(lightsBuffer, lights.size() * sizeof(LightUBO), lights.data(), GL_DYNAMIC_STORAGE_BIT);

	glCreateBuffers(1, &beltBuffer);
	glNamedBufferStorage(beltBuffer, sizeof(double) * Playback::magicCount, music.data(), GL_DYNAMIC_STORAGE_BIT);

	glCreateBuffers(1, &toonBuffer);
	glNamedBufferStorage(toonBuffer, sizeof(ToonUBO), &toonUbo, GL_DYNAMIC_STORAGE_BIT);

	playback = std::make_unique<Playback>("music/incompetech_hall-of-the-mountain-king.flac");
}

Application::~Application() {
	glDeleteTextures(1, &postprocessFramebufferDepth);
	glDeleteTextures(1, &postprocessFramebufferColor);
	glDeleteTextures(1, &postprocessFramebuffer);

	glDeleteBuffers(1, &lightsBuffer);
	glDeleteBuffers(1, &cameraBuffer);
}

void Application::render() {
	time = glfwGetTime();
	// --------------------------------------------------------------------------
	// Update data
	// --------------------------------------------------------------------------
	pioneer->update();
	voyager->update();
	ulysses->update();
	galileo->update();
	cassini->update();
	juno->update();

	// Camera
	if (followedProbe) {
		camera.center(followedProbe->position());
		camera.angleDirection(camera.angleDirection() - float(followedProbe->deltaAngle()));
	}
	cameraUbo.position = glm::vec4(camera.eyePosition(), 1.0f);
	cameraUbo.view = glm::lookAt(camera.eyePosition(), camera.center(), glm::vec3(0.0f, 1.0f, 0.0f));
	glNamedBufferSubData(cameraBuffer, 0, sizeof(CameraUBO), &cameraUbo);

	// Belt
	for (size_t i = 0; i < Playback::magicCount; i++) {
		if (playback->magic[i] > music[i]) {
			music[i] = playback->magic[i];
		} else if (playback->magic[i] < music[i]) {
			music[i] += Application::musicStepDown;
		}
	}
	glNamedBufferSubData(beltBuffer, 0, sizeof(double) * Playback::magicCount, music.data());

	// Toon Shading
	glNamedBufferSubData(toonBuffer, 0, sizeof(ToonUBO), &toonUbo);
	glBindBufferBase(GL_UNIFORM_BUFFER, 3, toonBuffer);

	// Lights
	glNamedBufferSubData(lightsBuffer, 0, lights.size() * sizeof(LightUBO), lights.data());

	// --------------------------------------------------------------------------
	// Draw the scene
	// --------------------------------------------------------------------------

	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, this->width, this->height);

	// Configure fixed function pipeline
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, cameraBuffer);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, lightsBuffer);

	// Draw the skybox
	skybox.draw();

	// Draw Jupiter
	glUseProgram(jupiterProgram.id());
	glBindBufferBase(GL_UNIFORM_BUFFER, 2, jupiterMaterial.buffer());
	glProgramUniformMatrix4fv(jupiterProgram.id(), 0, 1, GL_FALSE, glm::value_ptr(jupiterModelMatrix));
	glProgramUniform1d(jupiterProgram.id(), 1, time);
	glBindTextureUnit(0, jupiterTexture.id());
	polarTeapot.draw();

	// Draw the belt
	glUseProgram(beltProgram.id());
	glBindBufferBase(GL_UNIFORM_BUFFER, 2, beltMaterial.buffer());
	glBindBufferBase(GL_UNIFORM_BUFFER, 4, beltBuffer);
	glProgramUniformMatrix4fv(beltProgram.id(), 0, 1, GL_FALSE, glm::value_ptr(beltModelMatrix));
	glProgramUniform1d(beltProgram.id(), 1, time);
	glBindTextureUnit(0, ringsTexture.id());
	glBindVertexArray(teapot.vao());
	glDrawElementsInstanced(teapot.mode(), teapot.indicesCount(), GL_UNSIGNED_INT, nullptr, 2048);

	// Draw the probes
	pioneer->draw();
	voyager->draw();
	ulysses->draw();
	galileo->draw();
	cassini->draw();
	juno->draw();

	lastTime = time;
}

void Application::onResize(GLFWwindow *window, int width, int height) {
	this->width = width;
	this->height = height;
	glViewport(0, 0, width, height);

	//glDeleteTextures(1, &postprocessFramebufferColor);
	//glDeleteTextures(1, &postprocessFramebufferDepth);

	//// Re-Initialize color output texture
	//glCreateTextures(GL_TEXTURE_2D, 1, &postprocessFramebufferColor);
	//glTextureStorage2D(postprocessFramebufferColor, 1, GL_RGBA32F, width, height);

	//// Re-Initialize depth output texture
	//glCreateTextures(GL_TEXTURE_2D, 1, &postprocessFramebufferDepth);
	//glTextureStorage2D(postprocessFramebufferDepth, 1, GL_DEPTH_COMPONENT32F, width, height);

	//// Re-Associate color and depth `attachments` with color and depth `textures`
	//glNamedFramebufferTexture(postprocessFramebuffer, GL_COLOR_ATTACHMENT0, postprocessFramebufferColor, 0);
	//glNamedFramebufferTexture(postprocessFramebuffer, GL_DEPTH_ATTACHMENT, postprocessFramebufferDepth, 0);
}
void Application::onMouseMove(GLFWwindow *window, double x, double y) {
	camera.onMouseMove(x, y);
}
void Application::onMousePressed(GLFWwindow *window, int button, int action, int mods) {
	camera.onMouseButton(button, action, mods);
}
void Application::onKeyPressed(GLFWwindow *window, int key, int scancode, int action, int mods) {
	switch (key) {
	case GLFW_KEY_0:
		followedProbe = nullptr;
		camera.center(glm::vec3(0.0f));
		break;
	case GLFW_KEY_1:
		followedProbe = pioneer.get();
		break;
	case GLFW_KEY_2:
		followedProbe = voyager.get();
		break;
	case GLFW_KEY_3:
		followedProbe = ulysses.get();
		break;
	case GLFW_KEY_4:
		followedProbe = galileo.get();
		break;
	case GLFW_KEY_5:
		followedProbe = cassini.get();
		break;
	case GLFW_KEY_6:
		followedProbe = juno.get();
		break;
	case GLFW_KEY_T:
		if (action == GLFW_PRESS) {
			toonUbo.isEnabled = !toonUbo.isEnabled;
		}
		break;
	}
}
