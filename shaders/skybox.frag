#version 450

layout(binding = 0) uniform samplerCube skybox;

layout(location = 0) in vec3 fs_TextureCoord;

layout(location = 0) out vec4 finalColor;

layout(binding = 3, std140) uniform Toon {
	int levels;
	uint isEnabled;
} toon;

void main()
{
    vec4 color = texture(skybox, fs_TextureCoord);
	if (toon.isEnabled > 0) {
		float light = sqrt(color.r * color.r + color.g * color.g+ color.b * color.b);
		light = floor(light * toon.levels) / toon.levels;
		color.rgb *= light;
	}
	finalColor = color;
}
