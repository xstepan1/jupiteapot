#version 450

// ---------------------------------------------------------------------------------
// Obtained from: https://github.com/ashima/webgl-noise/blob/master/src/noise2D.glsl
// ---------------------------------------------------------------------------------

// Description : Array and textureless GLSL 2D simplex noise function.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
// 

vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec2 mod289(vec2 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec3 permute(vec3 x) {
  return mod289(((x*34.0)+1.0)*x);
}

float snoise(vec2 v)
  {
  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                     -0.577350269189626,  // -1.0 + 2.0 * C.x
                      0.024390243902439); // 1.0 / 41.0
// First corner
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);

// Other corners
  vec2 i1;
  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
  //i1.y = 1.0 - i1.x;
  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
  // x0 = x0 - 0.0 + 0.0 * C.xx ;
  // x1 = x0 - i1 + 1.0 * C.xx ;
  // x2 = x0 - 1.0 + 2.0 * C.xx ;
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;

// Permutations
  i = mod289(i); // Avoid truncation effects in permutation
  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
		+ i.x + vec3(0.0, i1.x, 1.0 ));

  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
  m = m*m ;
  m = m*m ;

// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

  vec3 x = 2.0 * fract(p * C.www) - 1.0;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;

// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );

// Compute final noise value at P
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}

// ---------------------
// End of 3rd party code
// ---------------------

layout(binding = 0, std140) uniform Camera {
	mat4 projection;
	mat4 view;
	vec3 position;
} camera;

struct Light {
	vec4 position;
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
};

layout(binding = 1, std430) buffer Lights {
	Light lights[];
};

layout(binding = 2, std140) uniform Material {
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
} material;

layout(binding = 4, std140) uniform Belt {
	double magic[64];
};

layout(location = 0) uniform mat4 modelMatrix;
layout(location = 1) uniform double time;

layout(binding = 0) uniform sampler2D diffuseTexture;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

layout(location = 0) out vec3 fs_Position;
layout(location = 1) out vec3 fs_Normal;
layout(location = 2) out vec4 fs_RingColor;

void main()
{
	float angleY = snoise(vec2(gl_InstanceID, 0.0f) * 100.0f) * 3.1415f;
	angleY += sign(angleY) * float(time) * 10.0f;
	mat4 modelRotationY = mat4(
		cos(angleY), 0.0f, -sin(angleY), 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		sin(angleY), 0.0f, cos(angleY), 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	float dist = snoise(vec2(gl_InstanceID, 0.0f));
	float music = float(magic[int(dist * 32.0f + 16.0f) ]);
	vec2 direction = vec2(cos(gl_InstanceID), sin(gl_InstanceID)) * (15.0f + dist * 5.0f);
	mat4 modelTranslation = mat4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		direction.x, music, direction.y, 1.0f);

	float angleRing = float(time) / 10.0f;
	mat4 modelRotationRing = mat4(
		cos(angleRing), 0.0f, -sin(angleRing), 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		sin(angleRing), 0.0f, cos(angleRing), 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	mat4 model = modelRotationRing * modelTranslation * modelRotationY * modelMatrix;

	fs_Position = vec3(model * vec4(position, 1.0));
	fs_Normal = transpose(inverse(mat3(model))) * normal;
	fs_RingColor = texture(diffuseTexture, vec2(0.0f, dist));
	gl_Position = camera.projection * camera.view * model * vec4(position, 1.0f);
}
