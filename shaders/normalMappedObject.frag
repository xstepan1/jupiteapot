#version 450

layout(binding = 0, std140) uniform Camera {
	mat4 projection;
	mat4 view;
	vec3 position;
} camera;

struct Light {
	vec4 position;
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
};

layout(binding = 1, std430) buffer Lights {
	Light lights[];
};

layout(binding = 2, std140) uniform Material {
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
} material;

layout(binding = 3, std140) uniform Toon {
	int levels;
	uint isEnabled;
} toon;

layout(binding = 0) uniform sampler2D diffuseTexture;
layout(binding = 3) uniform sampler2D normalTexture;

layout(location = 0) uniform mat4 modelMatrix;

layout(location = 0) in mat3 fs_TBN;
layout(location = 3) in vec3 fs_Position;
layout(location = 4) in vec2 fs_TextureCoord;
layout(location = 5) in vec3 fs_CameraPosition;

layout(location = 0) out vec4 finalColor;

void main()
{
	vec3 lightSum = vec3(0.0f);
	for(int i = 0; i <= lights.length(); i++) {
		Light light = lights[i];

		vec3 lightVector = fs_TBN * light.position.xyz - fs_Position * light.position.w;
		vec3 L = normalize(lightVector);
		vec3 E = normalize(fs_CameraPosition - fs_Position);
		vec3 H = normalize(L + E);
		vec3 N = texture(normalTexture, fs_TextureCoord).rgb;
		N = normalize(N * 2.0 - 1.0);
		 
		float NdotL = max(dot(N, L), 0.0);
		if (toon.isEnabled > 0) {
			NdotL = floor(NdotL * toon.levels) * 1.0 / toon.levels;
		}
		float NdotH = max(dot(N, H), 0.0);

		float distance2 = light.position.w == 1.0 ? pow(length(lightVector), 2) : 1.0;

		vec3 ambient = material.ambientColor.rgb * light.ambientColor.rgb;
		vec3 diffuse = texture(diffuseTexture, fs_TextureCoord).rgb * material.diffuseColor.rgb * light.diffuseColor.rgb;
		vec3 specular = material.specularColor.rgb * light.specularColor.rgb;

		float spec = pow(NdotH, material.specularColor.w);
		if (toon.isEnabled > 0) {
			spec = floor(spec * toon.levels) * 1.0 / toon.levels;
		}

		vec3 color = ambient.rgb
			+ NdotL * diffuse.rgb
			+ spec * specular;
		color /= distance2;

		lightSum += color;
	}
	finalColor = vec4(lightSum, 1.0);
}
