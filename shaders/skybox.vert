#version 450

layout(binding = 0, std140) uniform Camera {
	mat4 projection;
	mat4 view;
	vec3 position;
} camera;

layout(location = 0) in vec3 position;

layout(location = 0) out vec3 fs_TextureCoord;

void main()
{
	fs_TextureCoord = position;
	gl_Position = camera.projection * camera.view * vec4(position * 256.0, 1.0);
}
