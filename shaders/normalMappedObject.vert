#version 450

layout(binding = 0, std140) uniform Camera {
	mat4 projection;
	mat4 view;
	vec3 position;
} camera;

struct Light {
	vec4 position;
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
};

layout(binding = 1, std430) buffer Lights {
	Light lights[];
};

layout(binding = 2, std140) uniform Material {
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
} material;

layout(location = 0) uniform mat4 modelMatrix;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 textureCoord;
layout(location = 3) in vec3 tangent;

layout(location = 0) out mat3 fs_TBN;
layout(location = 3) out vec3 fs_Position;
layout(location = 4) out vec2 fs_TextureCoord;
layout(location = 5) out vec3 fs_CameraPosition;

void main()
{
	mat3 normalMatrix = transpose(inverse(mat3(modelMatrix)));
	vec3 T = normalize(normalMatrix * tangent);
	vec3 N = normalize(normalMatrix * normal);
	vec3 B = cross(N, T);

	fs_TBN = transpose(mat3(T,B,N));
	fs_Position = fs_TBN * vec3(modelMatrix * vec4(position, 1.0));
	fs_TextureCoord = textureCoord;
	fs_CameraPosition = fs_TBN * camera.position;

	gl_Position = camera.projection * camera.view * modelMatrix * vec4(position, 1.0);
}
