#version 450

layout(location = 0) in vec3 fs_Position;
layout(location = 1) in vec3 fs_Color;

layout(location = 0) out vec4 finalColor;

void main()
{
	finalColor = vec4(fs_Color, 1.0);
}
