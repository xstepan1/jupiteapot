#version 450

layout(binding = 0, std140) uniform Camera {
	mat4 projection;
	mat4 view;
	vec3 position;
} camera;

struct Light {
	vec4 position;
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
};

layout(binding = 1, std430) buffer Lights {
	Light lights[];
};

layout(binding = 2, std140) uniform Material {
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
} material;

layout(location = 0) uniform mat4 modelMatrix;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 textureCoord;

layout(location = 0) out vec3 fs_Position;
layout(location = 1) out vec3 fs_Normal;
layout(location = 2) out vec2 fs_TextureCoord;

void main()
{
	fs_Position = vec3(modelMatrix * vec4(position, 1.0));
	fs_Normal = transpose(inverse(mat3(modelMatrix))) * normal;
	fs_TextureCoord = textureCoord;

	gl_Position = camera.projection * camera.view * modelMatrix * vec4(position, 1.0);
}
