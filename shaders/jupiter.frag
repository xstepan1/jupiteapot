#version 450

// ---------------------------------------------------------------------------------
// Obtained from: https://github.com/ashima/webgl-noise/blob/master/src/noise3D.glsl
// ---------------------------------------------------------------------------------

//
// Description : Array and textureless GLSL 2D/3D/4D simplex 
//               noise functions.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
// 

vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 mod289(vec4 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 permute(vec4 x) {
     return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(vec4 r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}

float snoise(vec3 v)
{
  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
  const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

// First corner
  vec3 i  = floor(v + dot(v, C.yyy) );
  vec3 x0 =   v - i + dot(i, C.xxx) ;

// Other corners
  vec3 g = step(x0.yzx, x0.xyz);
  vec3 l = 1.0 - g;
  vec3 i1 = min( g.xyz, l.zxy );
  vec3 i2 = max( g.xyz, l.zxy );

  //   x0 = x0 - 0.0 + 0.0 * C.xxx;
  //   x1 = x0 - i1  + 1.0 * C.xxx;
  //   x2 = x0 - i2  + 2.0 * C.xxx;
  //   x3 = x0 - 1.0 + 3.0 * C.xxx;
  vec3 x1 = x0 - i1 + C.xxx;
  vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y
  vec3 x3 = x0 - D.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y

// Permutations
  i = mod289(i); 
  vec4 p = permute( permute( permute( 
             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
           + i.y + vec4(0.0, i1.y, i2.y, 1.0 )) 
           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

// Gradients: 7x7 points over a square, mapped onto an octahedron.
// The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)
  float n_ = 0.142857142857; // 1.0/7.0
  vec3  ns = n_ * D.wyz - D.xzx;

  vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)

  vec4 x_ = floor(j * ns.z);
  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

  vec4 x = x_ *ns.x + ns.yyyy;
  vec4 y = y_ *ns.x + ns.yyyy;
  vec4 h = 1.0 - abs(x) - abs(y);

  vec4 b0 = vec4( x.xy, y.xy );
  vec4 b1 = vec4( x.zw, y.zw );

  //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;
  //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;
  vec4 s0 = floor(b0)*2.0 + 1.0;
  vec4 s1 = floor(b1)*2.0 + 1.0;
  vec4 sh = -step(h, vec4(0.0));

  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
  vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

  vec3 p0 = vec3(a0.xy,h.x);
  vec3 p1 = vec3(a0.zw,h.y);
  vec3 p2 = vec3(a1.xy,h.z);
  vec3 p3 = vec3(a1.zw,h.w);

//Normalise gradients
  vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
  p0 *= norm.x;
  p1 *= norm.y;
  p2 *= norm.z;
  p3 *= norm.w;

// Mix final noise value
  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
  m = m * m;
  return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), 
                                dot(p2,x2), dot(p3,x3) ) );
}

// ---------------------------------------------------------------------------------------------------------
// Appropriated from: https://www.seedofandromeda.com/blogs/49-procedural-gas-giant-rendering-with-gpu-noise
// ---------------------------------------------------------------------------------------------------------

float noise(vec3 position, int octaves, float frequency, float persistence)
{
	float total = 0.0; // Total value so far
	float maxAmplitude = 0.0; // Accumulates highest theoretical amplitude
	float amplitude = 1.0;
	for (int i = 0; i < octaves; i++) {

		// Get the noise sample
		total += snoise(position * frequency) * amplitude;

		// Make the wavelength twice as small
		frequency *= 2.0;

		// Add to our maximum possible amplitude
		maxAmplitude += amplitude;

		// Reduce amplitude according to persistence for the next octave
		amplitude *= persistence;
	}

	// Scale the result by the maximum amplitude
	return total / maxAmplitude;
}

float ridgedNoise(vec3 position, int octaves, float frequency, float persistence)
{
	float total = 0.0; // Total value so far
	float maxAmplitude = 0.0; // Accumulates highest theoretical amplitude
	float amplitude = 1.0;
	for (int i = 0; i < octaves; i++) {

		// Get the noise sample
		total += ((1.0 - abs(snoise(position * frequency))) * 2.0 - 1.0) * amplitude;

		// Make the wavelength twice as small
		frequency *= 2.0;

		// Add to our maximum possible amplitude
		maxAmplitude += amplitude;

		// Reduce amplitude according to persistence for the next octave
		amplitude *= persistence;
	}

	// Scale the result by the maximum amplitude
	return total / maxAmplitude;
}


// ---------------------
// End of 3rd party code
// ---------------------

layout(binding = 0, std140) uniform Camera {
	mat4 projection;
	mat4 view;
	vec3 position;
} camera;

struct Light {
	vec4 position;
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
};

layout(binding = 1, std430) buffer Lights {
	Light lights[];
};

layout(binding = 2, std140) uniform Material {
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
} material;

layout(binding = 3, std140) uniform Toon {
	int levels;
	uint isEnabled;
} toon;

layout(binding = 0) uniform sampler2D diffuseTexture;

layout(location = 1) uniform double time;

layout(location = 0) in vec3 fs_Position;
layout(location = 1) in vec3 fs_Normal;
layout(location = 2) in vec2 fs_TextureCoord;

layout(location = 0) out vec4 finalColor;

vec3 computeLights()
{
	vec3 lightsSum = vec3(0.0);
	for(int i = 0; i < lights.length(); i++) {
		Light light = lights[i];

		vec3 lightVector = light.position.xyz - fs_Position * light.position.w;
		vec3 L = normalize(lightVector);
		vec3 N = normalize(fs_Normal);
		vec3 E = normalize(camera.position - fs_Position); 
		vec3 H = normalize(L + E);

		float NdotL = max(dot(N, L), 0.0);
		if (toon.isEnabled > 0) {
			NdotL = floor(NdotL * toon.levels) * 1.0 / toon.levels;
		}
		float NdotH = max(dot(N, H), 0.0);

		float distance2 = light.position.w == 1.0 ? pow(length(lightVector), 2) : 1.0;

		vec3 ambient = material.ambientColor.rgb * light.ambientColor.rgb;
		vec3 diffuse = material.diffuseColor.rgb * light.diffuseColor.rgb;
		vec3 specular = material.specularColor.rgb * light.specularColor.rgb;

		float spec = pow(NdotH, material.specularColor.w);
		if (toon.isEnabled > 0) {
			spec = floor(spec * toon.levels) * 1.0 / toon.levels;
		}

		vec3 color = ambient.rgb
			+ NdotL * diffuse.rgb
			+ spec * specular;
		color /= distance2;

		lightsSum += color;
	}
	return lightsSum;
}

float computeStormNoise(vec3 normal)
{
	// Get the three threshold samples
	float s = 0.5;
	float t1 = snoise(normal * 6.0) - s;
	float t2 = snoise((normal + 800.0) * 6.0) - s;
	float t3 = snoise((normal + 1600.0) * 6.0) - s;
	// Intersect them and get rid of negatives
	float threshold = max(t1 * t2 * t3, 0.0);
//	return threshold * 3.0;
	return snoise(normal * 0.1) * threshold;
}

void main()
{
	vec3 noiseNormal = fs_Normal + vec3(time / 100.0, 0.0, time/ 100.0);
	float noise = noise(noiseNormal, 6, 10.0, 0.8) * 0.02;
	noise += ridgedNoise(noiseNormal, 5, 5.8, 0.75) * 0.03 - 0.01;
	noise += computeStormNoise(noiseNormal) * 1.5;

	vec2 textureCoord = vec2(fs_TextureCoord.x, fs_TextureCoord.y + noise);
	vec3 textureColor = texture(diffuseTexture, textureCoord).rgb;
	finalColor = vec4(textureColor * computeLights(), 1.0);
//	finalColor.r += computeStormNoise(noiseNormal);
}
