#version 450

layout(binding = 0, std140) uniform Camera {
	mat4 projection;
	mat4 view;
	vec3 position;
} camera;

struct Light {
	vec4 position;
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
};

layout(binding = 1, std430) buffer Lights {
	Light lights[];
};

layout(location = 0) in vec3 position;

layout(location = 0) out vec3 fs_Position;
layout(location = 1) out vec3 fs_Color;

void main()
{
	vec4 lightPosition = lights[gl_InstanceID].position;

	fs_Position = position * 0.1 + lightPosition.xyz;
	// we can get rid of directional light this way
	fs_Position *= lightPosition.w;
	fs_Color = lights[gl_InstanceID].diffuseColor.rgb;

	gl_Position = camera.projection * camera.view * vec4(fs_Position, 1.0);
}
