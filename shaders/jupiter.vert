#version 450

layout(binding = 0, std140) uniform Camera {
	mat4 projection;
	mat4 view;
	vec3 position;
} camera;

struct Light {
	vec4 position;
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
};

layout(binding = 1, std430) buffer Lights {
	Light lights[];
};

layout(binding = 2, std140) uniform Material {
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
} material;

layout(location = 0) uniform mat4 modelMatrix;
layout(location = 1) uniform double time;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 textureCoord;

layout(location = 0) out vec3 fs_Position;
layout(location = 1) out vec3 fs_Normal;
layout(location = 2) out vec2 fs_TextureCoord;

void main()
{
	float angle = float(time / 100.0);
	mat4 modelRotation = mat4(
		cos(angle), 0.0f, -sin(angle), 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		sin(angle), 0.0f, cos(angle), 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	mat4 model = modelRotation * modelMatrix;

	fs_Position = vec3(model * vec4(position, 1.0));
	fs_Normal = transpose(inverse(mat3(model))) * normal;
	fs_TextureCoord = textureCoord;

	gl_Position = camera.projection * camera.view * model * vec4(position, 1.0);
}
