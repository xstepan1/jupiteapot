#pragma once

#include "material.hpp"
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <memory>
#include <string>
#include <vector>

class Mesh {
public:
	Mesh(std::vector<float> vertices,
	    Material* material = nullptr)
	    : Mesh(vertices,
	          std::vector<float>(),
	          std::vector<float>(),
	          std::vector<uint32_t>(),
	          material) {
	}
	Mesh(std::vector<float> vertices,
	    std::vector<uint32_t> indices,
	    Material* material = nullptr)
	    : Mesh(vertices,
	          std::vector<float>(),
	          std::vector<float>(),
	          indices,
	          material) {
	}
	Mesh(std::vector<float> vertices,
	    std::vector<float> normals,
	    std::vector<uint32_t> indices,
	    Material* material = nullptr)
	    : Mesh(vertices,
	          normals,
	          std::vector<float>(),
	          indices,
	          material) {}

	Mesh(std::vector<float> vertices,
	    std::vector<float> normals,
	    std::vector<float> textureCoords,
	    std::vector<uint32_t> indices,
	    Material* material = nullptr)
	    : Mesh(vertices,
	          normals,
	          textureCoords,
	          indices,
	          std::vector<float>(),
	          material) {}

	Mesh(std::vector<float> vertices,
	    std::vector<float> normals,
	    std::vector<float> textureCoords,
	    std::vector<uint32_t> indices,
	    std::vector<float> tangents,
	    Material* material = nullptr);
	Mesh(const Mesh& other);
	~Mesh();

	void createVao();

	void draw();

	static Mesh cube();
	static Mesh sphere();
	static Mesh teapot();
	static Mesh polarTeapot();

	GLuint vao() const {
		return this->_vao;
	}

	GLenum mode() const {
		return this->_mode;
	}

	void mode(GLenum value) {
		_mode = value;
	}

	size_t verticesCount() const {
		return this->_verticesCount;
	}

	size_t indicesCount() const {
		return this->_indicesCount;
	}

	GLuint verticesBuffer() const {
		return this->_verticesBuffer;
	}

	GLuint normalsBuffer() const {
		return this->_normalsBuffer;
	}

	GLuint texCoordsBuffer() const {
		return this->_textureCoordsBuffer;
	}

	GLuint indicesBuffer() const {
		return this->_indicesBuffer;
	}

	GLint positionLocation() const {
		return _positionLocation;
	}

	void positionLocation(GLint value) {
		_positionLocation = value;
	}

	GLint normalLocation() const {
		return _normalLocation;
	}

	void normalLocation(GLint value) {
		_normalLocation = value;
	}

	GLint textureCoordLocation() const {
		return _textureCoordLocation;
	}

	void textureCoordLocation(GLint value) {
		_textureCoordLocation = value;
	}

	Material* material() {
		return _material;
	}

	const glm::vec3& center() const {
		return _center;
	}

private:
	GLuint _vao = 0;

	size_t _verticesCount = 0;
	GLuint _verticesBuffer = 0;
	GLuint _normalsBuffer = 0;
	GLuint _textureCoordsBuffer = 0;
	GLuint _tangentsBuffer = 0;

	size_t _indicesCount = 0;
	GLuint _indicesBuffer = 0;

	GLint _positionLocation = 0;
	GLint _normalLocation = 1;
	GLint _textureCoordLocation = 2;
	GLint _tangentLocation = 3;

	GLenum _mode = GL_TRIANGLES;

	Material* _material = nullptr;

	glm::vec3 _center = glm::vec3(0.0);
};
