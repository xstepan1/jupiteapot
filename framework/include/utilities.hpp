#pragma once

#include <fstream>
#include <string>
#include <vector>
#include <tuple>

const std::string loadFile(const std::string &file_name);
std::tuple<std::vector<float>, std::vector<float>, std::vector<float>> deinterleave(const std::vector<float>& interleavedVertices);
std::vector<float> getPolarTextureCoords(const std::vector<float>& vertices);
