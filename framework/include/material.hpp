#pragma once

#include "texture.hpp"
#include <glad/glad.h>
#include <glm/glm.hpp>

class Material {
private:
	struct MaterialUBO {
		glm::vec4 ambientColor;
		glm::vec4 diffuseColor;
		glm::vec4 specularColor;
	};

public:
	Material()
	    : Material(glm::vec4(0.0f), glm::vec4(1.0f), glm::vec4(1.0f), glm::vec4(0.0f)) {}

	Material(const glm::vec4& ambientColor,
	    const glm::vec4& diffuseColor,
	    const glm::vec4& specularColor,
		const glm::vec4& emissiveColor,
	    Texture* diffuseTexture = nullptr,
	    Texture* normalTexture = nullptr)
	    : _ubo(MaterialUBO { ambientColor, diffuseColor, specularColor })
	    , _emissiveColor(emissiveColor)
	    , _diffuseTexture(diffuseTexture)
	    , _normalTexture(normalTexture) {
		glCreateBuffers(1, &_buffer);
		glNamedBufferStorage(_buffer, sizeof(MaterialUBO), &_ubo, GL_DYNAMIC_STORAGE_BIT);
	}

	~Material() {
		glDeleteBuffers(1, &_buffer);
	}

	const glm::vec4& ambientColor() const {
		return _ubo.ambientColor;
	}

	void ambientColor(const glm::vec4& value) {
		_ubo.ambientColor = value;
		glNamedBufferSubData(_buffer, 0, sizeof(MaterialUBO), &_ubo);
	}

	const glm::vec4& diffuseColor() const {
		return _ubo.diffuseColor;
	}

	void diffuseColor(const glm::vec4& value) {
		_ubo.diffuseColor = value;
		glNamedBufferSubData(_buffer, 0, sizeof(MaterialUBO), &_ubo);
	}

	const glm::vec4& specularColor() const {
		return _ubo.specularColor;
	}
	void specularColor(const glm::vec4& value) {
		_ubo.specularColor = value;
		glNamedBufferSubData(_buffer, 0, sizeof(MaterialUBO), &_ubo);
	}

	const glm::vec4& emissiveColor() const {
		return _emissiveColor;
	}

	void emissiveColor(const glm::vec4& value) {
		_emissiveColor = value;
	}

	Texture* diffuseTexture() {
		return _diffuseTexture;
	}

	Texture* normalTexture() {
		return _normalTexture;
	}

	GLuint buffer() const {
		return _buffer;
	}

private:
	MaterialUBO _ubo { glm::vec4(0.0f), glm::vec4(1.0f), glm::vec4(1.0f) };
	glm::vec4 _emissiveColor;
	Texture* _diffuseTexture = nullptr;
	Texture* _normalTexture = nullptr;
	GLuint _buffer = 0;
};