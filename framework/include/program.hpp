#pragma once

#include "utilities.hpp"
#include <glad/glad.h>

class Program {
public:
	Program(const std::string &vertexPath, const std::string &fragmentPath);
	~Program();

private:
	GLuint _id;

public:
	GLuint id() const {
		return _id;
	}
};
