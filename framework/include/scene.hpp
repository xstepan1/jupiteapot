#pragma once

#include "material.hpp"
#include "mesh.hpp"
#include "texture.hpp"
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

class Scene {
public:
	Scene(const std::string& objPath);

	std::vector<std::unique_ptr<Mesh>>& meshes() {
		return _meshes;
	}

private:
	std::vector<std::unique_ptr<Mesh>> _meshes;
	std::vector<std::unique_ptr<Texture>> _textures;
	std::vector<std::unique_ptr<Material>> _materials;

	Texture* loadTexture(std::unordered_map<std::string, Texture&>& cache, const std::string& directory, const std::string& path);
};
