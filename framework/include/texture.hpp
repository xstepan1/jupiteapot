#pragma once

#include <glad/glad.h>
#include <string>

class Texture {
public:
	Texture(const std::string& path);
	Texture(const std::string& front,
	    const std::string& back,
	    const std::string& up,
	    const std::string& down,
	    const std::string& right,
	    const std::string& left);
	~Texture();

private:
	GLuint _id;

public:
	GLuint id() {
		return _id;
	}
};
