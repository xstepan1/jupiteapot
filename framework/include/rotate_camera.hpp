#pragma once

#include <glm/glm.hpp>

/// This is a VERY SIMPLE class that allows to very simply move with the camera.
/// It is not a perfect, brilliant, smart, or whatever implementation of a camera,
/// but it is sufficient for PV112 lectures.
///
/// Use left mouse button to change the point of view.
/// Use right mouse button to zoom in and zoom out.
class RotateCamera {
private:
	/// Constants that defines the behaviour of the camera
	///		- Minimum elevation in radians
	static const float minElevation;
	///		- Maximum elevation in radians
	static const float maxElevation;
	///		- Minimum distance from the point of interest
	static const float minDistance;
	///		- Sensitivity of the mouse when changing elevation or direction angles
	static const float angleSensitivity;
	///		- Sensitivity of the mouse when changing zoom
	static const float zoomSensitivity;

	/// angle_direction is an angle in which determines into which direction in xz plane I look.
	///		- 0 degrees .. I look in -z direction
	///		- 90 degrees .. I look in -x direction
	///		- 180 degrees .. I look in +z direction
	///		- 270 degrees .. I look in +x direction
	float _angleDirection = 0.0f;

	/// angle_direction is an angle in which determines from which "height" I look.
	///		- positive elevation .. I look from above the xz plane
	///		- negative elevation .. I look from below the xz plane
	float _angleElevation = 0.5f;

	/// Distance from (0,0,0), the point at which I look
	float _distance = 50.0f;

	/// Final position of the eye in world space coordinates, for LookAt or shaders
	glm::vec3 _eyePosition;

	/// Last X and Y coordinates of the mouse cursor
	int _lastX = 0;
	int _lastY = 0;

	/// True or false if moused buttons are pressed and the user rotates/zooms the camera
	bool _isRotating = false;
	bool _isZooming = false;

	glm::vec3 _center = glm::vec3(0.0f, 0.0f, 0.0f);

	/// Recomputes 'eye_position' from 'angle_direction', 'angle_elevation', and 'distance'
	void updateEyePos();

public:
	RotateCamera();

	/// Call when the user presses or releases a mouse button (see glfwSetMouseButtonCallback)
	void onMouseButton(int button, int action, int mods);

	/// Call when the user moves with the mouse cursor (see glfwSetCursorPosCallback)
	void onMouseMove(double x, double y);

	/// Returns the position of the eye in world space coordinates
	glm::vec3 eyePosition() const {
		return _eyePosition;
	}

	glm::vec3 center() const {
		return _center;
	}

	void center(const glm::vec3 value) {
		_center = value;
		updateEyePos();
	}

	float angleDirection() const {
		return _angleDirection;
	}

	void angleDirection(float value) {
		_angleDirection = value;
		updateEyePos();
	}
};
