#include "mesh.hpp"
#include "cube.inl"
#include "sphere.inl"
#include "teapot.inl"
#include "tiny_obj_loader.h"
#include "utilities.hpp"
#include <algorithm>
#include <stdexcept>

Mesh::Mesh(std::vector<float> vertices,
    std::vector<float> normals,
    std::vector<float> textureCoords,
    std::vector<uint32_t> indices,
    std::vector<float> tangents,
    Material* material)
    : _material(material)
    , _verticesCount(vertices.size() / 3) {

	glCreateBuffers(1, &_verticesBuffer);
	glNamedBufferStorage(_verticesBuffer, vertices.size() * sizeof(float), vertices.data(), GL_DYNAMIC_STORAGE_BIT);
	if (!vertices.empty()) {

		for (size_t i = 0; i < vertices.size(); i += 3) {
			_center += glm::vec3(vertices[i], vertices[i + 1], vertices[i + 2]);
		}
		_center /= _verticesCount;
	}

	if (!normals.empty()) {
		glCreateBuffers(1, &_normalsBuffer);
		glNamedBufferStorage(_normalsBuffer, normals.size() * sizeof(float), normals.data(), GL_DYNAMIC_STORAGE_BIT);
	}

	if (!textureCoords.empty()) {
		glCreateBuffers(1, &_textureCoordsBuffer);
		glNamedBufferStorage(_textureCoordsBuffer, textureCoords.size() * sizeof(float), textureCoords.data(), GL_DYNAMIC_STORAGE_BIT);
	}

	if (!indices.empty()) {
		glCreateBuffers(1, &_indicesBuffer);
		glNamedBufferStorage(_indicesBuffer, indices.size() * sizeof(float), indices.data(), GL_DYNAMIC_STORAGE_BIT);
		_indicesCount = indices.size();
	}

	if (!tangents.empty()) {
		glCreateBuffers(1, &_tangentsBuffer);
		glNamedBufferStorage(_tangentsBuffer, vertices.size() * sizeof(float), vertices.data(), GL_DYNAMIC_STORAGE_BIT);
	}

	createVao();
}

Mesh::Mesh(const Mesh& other)
    : _verticesCount(other._verticesCount)
    , _indicesCount(other._indicesCount)
    , _mode(other._mode)
    , _positionLocation(other._positionLocation)
    , _normalLocation(other._normalLocation)
    , _textureCoordLocation(other._textureCoordLocation)
    , _material(other._material) {

	if (other._verticesBuffer != 0) {
		glCreateBuffers(1, &_verticesBuffer);
		glNamedBufferStorage(_verticesBuffer, _verticesCount * sizeof(float) * 3, nullptr, GL_DYNAMIC_STORAGE_BIT);
		glCopyNamedBufferSubData(other._verticesBuffer, _verticesBuffer, 0, 0, _verticesCount * sizeof(float) * 3);
	}

	if (other._normalsBuffer != 0) {
		glCreateBuffers(1, &_normalsBuffer);
		glNamedBufferStorage(_normalsBuffer, _verticesCount * sizeof(float) * 3, nullptr, GL_DYNAMIC_STORAGE_BIT);
		glCopyNamedBufferSubData(other._normalsBuffer, _normalsBuffer, 0, 0, _verticesCount * sizeof(float) * 3);
	}

	if (other._textureCoordsBuffer != 0) {
		glCreateBuffers(1, &_textureCoordsBuffer);
		glNamedBufferStorage(_textureCoordsBuffer, _verticesCount * sizeof(float) * 2, nullptr, GL_DYNAMIC_STORAGE_BIT);
		glCopyNamedBufferSubData(other._textureCoordsBuffer, _textureCoordsBuffer, 0, 0, _verticesCount * sizeof(float) * 2);
	}

	if (other._indicesBuffer != 0) {
		glCreateBuffers(1, &_indicesBuffer);
		glNamedBufferStorage(_indicesBuffer, _indicesCount * sizeof(uint32_t), nullptr, GL_DYNAMIC_STORAGE_BIT);
		glCopyNamedBufferSubData(other._indicesBuffer, _indicesBuffer, 0, 0, _indicesCount * sizeof(uint32_t));
	}

	createVao();
}

void Mesh::createVao() {
	glDeleteVertexArrays(1, &_vao);
	glCreateVertexArrays(1, &_vao);

	if (_verticesBuffer) {
		glVertexArrayVertexBuffer(_vao, _positionLocation, _verticesBuffer, 0, 3 * sizeof(float));

		glEnableVertexArrayAttrib(_vao, _positionLocation);
		glVertexArrayAttribFormat(_vao, _positionLocation, 3, GL_FLOAT, GL_FALSE, 0);
		glVertexArrayAttribBinding(_vao, _positionLocation, _positionLocation);
	}

	if (_normalsBuffer) {
		glVertexArrayVertexBuffer(_vao, _normalLocation, _normalsBuffer, 0, 3 * sizeof(float));

		glEnableVertexArrayAttrib(_vao, _normalLocation);
		glVertexArrayAttribFormat(_vao, _normalLocation, 3, GL_FLOAT, GL_FALSE, 0);
		glVertexArrayAttribBinding(_vao, _normalLocation, _normalLocation);
	}

	if (_textureCoordsBuffer) {
		glVertexArrayVertexBuffer(_vao, _textureCoordLocation, _textureCoordsBuffer, 0, 2 * sizeof(float));

		glEnableVertexArrayAttrib(_vao, _textureCoordLocation);
		glVertexArrayAttribFormat(_vao, _textureCoordLocation, 2, GL_FLOAT, GL_FALSE, 0);
		glVertexArrayAttribBinding(_vao, _textureCoordLocation, _textureCoordLocation);
	}

	if (_tangentsBuffer) {
		glVertexArrayVertexBuffer(_vao, _tangentLocation, _tangentsBuffer, 0, 3 * sizeof(float));

		glEnableVertexArrayAttrib(_vao, _tangentLocation);
		glVertexArrayAttribFormat(_vao, _tangentLocation, 3, GL_FLOAT, GL_FALSE, 0);
		glVertexArrayAttribBinding(_vao, _tangentLocation, _tangentLocation);
	}

	if (_indicesBuffer) {
		glVertexArrayElementBuffer(_vao, _indicesBuffer);
	}
}

void Mesh::draw() {
	if (_material) {
		glBindBufferBase(GL_UNIFORM_BUFFER, 2, _material->buffer());
		if (_material->diffuseTexture()) {
			glBindTextureUnit(0, _material->diffuseTexture()->id());
		}
		if (_material->normalTexture()) {
			glBindTextureUnit(3, _material->normalTexture()->id());
		}
	}

	glBindVertexArray(_vao);

	if (_indicesBuffer > 0) {
		glDrawElements(_mode, static_cast<GLsizei>(_indicesCount), GL_UNSIGNED_INT, nullptr);
	} else {
		glDrawArrays(_mode, 0, static_cast<GLsizei>(_verticesCount));
	}
}

Mesh Mesh::cube() {
	auto vectors = deinterleave(std::vector<float>(cube_vertices, std::end(cube_vertices)));
	std::vector<uint32_t> indices(cube_indices, std::end(cube_indices));
	return Mesh(std::get<0>(vectors), std::get<1>(vectors), std::get<2>(vectors), indices);
}

Mesh Mesh::sphere() {
	auto vectors = deinterleave(std::vector<float>(cube_vertices, std::end(cube_vertices)));
	std::vector<uint32_t> indices(cube_indices, std::end(cube_indices));
	return Mesh(std::get<0>(vectors), std::get<1>(vectors), std::get<2>(vectors), indices);
}

Mesh Mesh::teapot() {
	auto vectors = deinterleave(std::vector<float>(teapot_vertices, std::end(teapot_vertices)));
	std::vector<uint32_t> indices(teapot_indices, std::end(teapot_indices));
	Mesh teapot(std::get<0>(vectors), std::get<1>(vectors), std::get<2>(vectors), indices);
	teapot.mode(GL_TRIANGLE_STRIP);
	return teapot;
}

Mesh Mesh::polarTeapot() {
	auto vectors = deinterleave(std::vector<float>(teapot_vertices, std::end(teapot_vertices)));
	std::vector<float> texCoords = getPolarTextureCoords(std::get<0>(vectors));
	Mesh mesh(std::get<0>(vectors),
	    std::get<1>(vectors),
	    texCoords,
	    std::vector<uint32_t>(teapot_indices, std::end(teapot_indices)));
	mesh.mode(GL_TRIANGLE_STRIP);
	mesh.createVao();
	return mesh;
}

Mesh::~Mesh() {
	glDeleteVertexArrays(1, &this->_vao);
	glDeleteBuffers(1, &this->_verticesBuffer);
	glDeleteBuffers(1, &this->_normalsBuffer);
	glDeleteBuffers(1, &this->_textureCoordsBuffer);
	glDeleteBuffers(1, &this->_indicesBuffer);
	glDeleteBuffers(1, &this->_tangentsBuffer);
}
