#include "program.hpp"
#include <stdexcept>


GLuint compileShader(const std::string &path, GLenum shaderType) {
	const std::string shaderSource = loadFile(path);
	const char *shaderData = shaderSource.data();

	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &shaderData, nullptr);
	glCompileShader(shader);

	GLint isCompiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_TRUE) {
		return shader;
	}

	GLint infoLogLength;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
	std::string infoLog(infoLogLength, '*');
	glGetShaderInfoLog(shader, infoLogLength, nullptr, infoLog.data());
	throw std::logic_error(infoLog);
}

Program::Program(const std::string &vertexPath, const std::string &fragmentPath) {
	GLuint vertexShader = compileShader(vertexPath, GL_VERTEX_SHADER);
	GLuint fragmentShader = compileShader(fragmentPath, GL_FRAGMENT_SHADER);

	_id = glCreateProgram();
	glAttachShader(_id, vertexShader);
	glAttachShader(_id, fragmentShader);
	glLinkProgram(_id);
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	glDetachShader(_id, vertexShader);
	glDetachShader(_id, fragmentShader);

	GLint isLinked;
	glGetProgramiv(_id, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_TRUE) {
		return;
	}
	GLint infoLogLength;
	glGetProgramiv(_id, GL_INFO_LOG_LENGTH, &infoLogLength);
	std::string infoLog(infoLogLength, '*');
	glGetProgramInfoLog(_id, infoLogLength, nullptr, infoLog.data());
	throw std::logic_error(infoLog);
}

Program::~Program() {
	glDeleteProgram(_id);
}