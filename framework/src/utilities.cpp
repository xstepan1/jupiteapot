#include "utilities.hpp"
#include <exception>

const std::string loadFile(const std::string &file_name) {
	std::ifstream infile { file_name };
	if (!infile) {
		throw std::runtime_error("File " + file_name + " does not exist.");
	}
	return { std::istreambuf_iterator<char>(infile), std::istreambuf_iterator<char>() };
}

std::tuple<std::vector<float>, std::vector<float>, std::vector<float>> deinterleave(const std::vector<float>& interleavedVertices) {
	std::vector<float> vertices;
	std::vector<float> normals;
	std::vector<float> tex_coords;

	for (size_t vertex_offset = 0; vertex_offset < interleavedVertices.size(); vertex_offset += 8) {
		for (int i = 0; i < 3; i++) {
			vertices.push_back(interleavedVertices[vertex_offset + i]);
			normals.push_back(interleavedVertices[vertex_offset + 3 + i]);
		}

		tex_coords.push_back(interleavedVertices[vertex_offset + 6]);
		tex_coords.push_back(interleavedVertices[vertex_offset + 7]);
	}
	return { vertices, normals, tex_coords };
}

std::vector<float> getPolarTextureCoords(const std::vector<float>& vertices) {
	if (vertices.size() < 2) {
		throw std::logic_error("Dude, there need to be at least some vertices.");
	}

	float min = vertices[1];
	float max = vertices[1];
	for (size_t i = 4; i < vertices.size(); i += 3) {
		if (vertices[i] < min) {
			min = vertices[i];
		}
		if (vertices[i] > max) {
			max = vertices[i];
		}
	}

	std::vector<float> texCoords;
	for (size_t i = 1; i < vertices.size(); i += 3) {
		texCoords.push_back(0.0f);
		texCoords.push_back((vertices[i] - min) / (max - min));
	}
	return texCoords;
}