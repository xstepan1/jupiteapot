#include "rotate_camera.hpp"

#include <GLFW/glfw3.h>

const float RotateCamera::minElevation = -1.5f;
const float RotateCamera::maxElevation = 1.5f;
const float RotateCamera::minDistance = 1.0f;
const float RotateCamera::angleSensitivity = 0.008f;
const float RotateCamera::zoomSensitivity = 0.003f;

RotateCamera::RotateCamera() {
	updateEyePos();
}

void RotateCamera::updateEyePos() {
	_eyePosition.x = _center.x + _distance * cosf(_angleElevation) * -sinf(_angleDirection);
	_eyePosition.y = _center.y + _distance * sinf(_angleElevation);
	_eyePosition.z = _center.z + _distance * cosf(_angleElevation) * cosf(_angleDirection);
}

void RotateCamera::onMouseButton(int button, int action, int mods) {
	// Left mouse button affects the angles
	if (button == GLFW_MOUSE_BUTTON_LEFT) {
		if (action == GLFW_PRESS) {
			_isRotating = true;
		} else {
			_isRotating = false;
		}
	}
	// Right mouse button affects the zoom
	if (button == GLFW_MOUSE_BUTTON_RIGHT) {
		if (action == GLFW_PRESS) {
			_isZooming = true;
		} else {
			_isZooming = false;
		}
	}
}

void RotateCamera::onMouseMove(double x, double y) {
	float dx = float(x - _lastX);
	float dy = float(y - _lastY);
	_lastX = static_cast<int>(x);
	_lastY = static_cast<int>(y);

	if (_isRotating) {
		_angleDirection += dx * angleSensitivity;
		_angleElevation += dy * angleSensitivity;

		// Clamp the results
		if (_angleElevation > maxElevation)
			_angleElevation = maxElevation;
		if (_angleElevation < minElevation)
			_angleElevation = minElevation;
	}
	if (_isZooming) {
		_distance *= (1.0f + dy * zoomSensitivity);

		// Clamp the results
		if (_distance < minDistance)
			_distance = minDistance;
	}
	updateEyePos();
}
