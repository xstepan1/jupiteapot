#include "texture.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <algorithm>
#include <cmath>
#include <stb_image.h>
#include <stdexcept>
#include <vector>

Texture::Texture(const std::string& filename) {

	int width, height, channels;
	stbi_set_flip_vertically_on_load(true);
	unsigned char* data = stbi_load(filename.c_str(), &width, &height, &channels, 4);

	glCreateTextures(GL_TEXTURE_2D, 1, &_id);

	glTextureStorage2D(_id,
	    std::max(std::log2(width), 1.0),
	    GL_RGBA8,
	    width,
	    height);

	glTextureSubImage2D(_id,
	    0, // level
	    0, // xoffset
	    0, // yoffset
	    width,
	    height,
	    GL_RGBA,
	    GL_UNSIGNED_BYTE,
	    data);

	stbi_image_free(data);

	glGenerateTextureMipmap(_id);

	glTextureParameteri(_id, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTextureParameteri(_id, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

Texture::Texture(const std::string& front,
    const std::string& back,
    const std::string& up,
    const std::string& down,
    const std::string& right,
    const std::string& left) {

	glCreateTextures(GL_TEXTURE_CUBE_MAP, 1, &_id);

	const char* faces[] {
		front.c_str(), back.c_str(), up.c_str(), down.c_str(), right.c_str(), left.c_str()
	};

	unsigned char* faceData[6];
	int width;
	int height;
	for (size_t i = 0; i < 6; i++) {
		int channels;
		const char* face = faces[i];
		faceData[i] = stbi_load(face, &width, &height, &channels, 4);
	}

	glTextureStorage2D(_id,
	    std::max(std::log2(width), 1.0),
	    GL_RGBA8,
	    width,
	    height);
	glTextureParameteri(_id, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTextureParameteri(_id, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTextureParameteri(_id, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTextureParameteri(_id, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTextureParameteri(_id, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	for (size_t i = 0; i < 6; i++) {

		glTextureSubImage3D(_id,
		    0, // level
		    0, // xoffset
		    0, // yoffset
		    i, // zoffset
		    width,
		    height,
		    1,
		    GL_RGBA,
		    GL_UNSIGNED_BYTE,
		    faceData[i]);

		stbi_image_free(faceData[i]);
		glGenerateTextureMipmap(_id);
	}
}

Texture::~Texture() {
	glDeleteTextures(1, &_id);
}