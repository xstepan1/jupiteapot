#include "scene.hpp"
#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"
#include <glm/glm.hpp>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <unordered_map>

// based on https://learnopengl.com/Advanced-Lighting/Normal-Mapping
glm::vec3 getTangent(glm::vec3 pos1,
    glm::vec3 pos2,
    glm::vec3 pos3,
    glm::vec2 tex1,
    glm::vec2 tex2,
    glm::vec2 tex3) {

	glm::vec3 edge1 = pos2 - pos1;
	glm::vec3 edge2 = pos3 - pos1;
	glm::vec2 deltaUV1 = tex2 - tex1;
	glm::vec2 deltaUV2 = tex3 - tex1;

	float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);
	return (edge1 * deltaUV2.y - edge2 * deltaUV1.y) * f;
}

Texture* Scene::loadTexture(std::unordered_map<std::string, Texture&>& cache,
    const std::string& directory,
    const std::string& path) {

	if (path.empty()) {
		return nullptr;
	}

	auto textureIt = cache.find(path);
	if (textureIt != cache.end()) {
		return &textureIt->second;
	}

	Texture& texture = *_textures.emplace_back(std::make_unique<Texture>(directory + path));
	cache.insert({ path, texture });
	return &texture;
}

Scene::Scene(const std::string& objPath) {
	std::string directory = objPath.substr(0, objPath.find_last_of("/\\"));
	directory.append("/");
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string err;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, objPath.c_str(), directory.c_str());

	if (!err.empty()) {
		throw std::logic_error(err);
	}

	if (!ret) {
		throw std::logic_error("An object could not be loaded.");
	}

	std::unordered_map<std::string, Texture&> textures;
	for (const tinyobj::material_t& material : materials) {
		Texture* diffuseTexture = loadTexture(textures, directory, material.diffuse_texname);
		Texture* normalTexture = loadTexture(textures, directory, material.normal_texname);
		glm::vec4 ambientColor(material.ambient[0], material.ambient[1], material.ambient[2], 0.0);
		glm::vec4 diffuseColor(material.diffuse[0], material.diffuse[1], material.diffuse[2], 0.0);
		glm::vec4 specularColor(material.specular[0], material.specular[1], material.specular[2], material.shininess);
		glm::vec4 emissiveColor(material.emission[0], material.emission[1], material.emission[2], 1.0);
		_materials.push_back(std::make_unique<Material>(ambientColor, diffuseColor, specularColor, emissiveColor, diffuseTexture, normalTexture));
	}

	for (const tinyobj::shape_t& shape : shapes) {
		std::vector<float> vertices;
		std::vector<float> normals;
		std::vector<float> textureCoords;
		std::vector<float> tangents;

		// unwrap data from each face
		for (size_t f = 0; f < shape.mesh.num_face_vertices.size(); f++) {
			// there are always 3 vertices in a polygon since `triangulate = true`
			for (size_t v = 0; v < 3; v++) {
				tinyobj::index_t idx = shape.mesh.indices[f * 3 + v];

				// unwrap vertices and optionally normals
				for (int i = 0; i < 3; i++) {
					vertices.push_back(attrib.vertices[3 * (size_t)idx.vertex_index + i]);

					if (!attrib.normals.empty()) {
						normals.push_back(attrib.normals[3 * (size_t)idx.normal_index + i]);
					}
				}

				// optionally unwrap texCoords
				if (!attrib.texcoords.empty()) {
					textureCoords.push_back(attrib.texcoords[2 * (size_t)idx.texcoord_index + 0]);
					textureCoords.push_back(attrib.texcoords[2 * (size_t)idx.texcoord_index + 1]);
				}
			}
		}

		if (!textureCoords.empty()) {
			for (size_t i = 0; i < shape.mesh.num_face_vertices.size(); i++) {
				glm::vec3 pos1(vertices[i * 3], vertices[(i * 3) + 1], vertices[(i * 3) + 2]);
				glm::vec3 pos2(vertices[(i * 3) + 3], vertices[(i * 3) + 4], vertices[(i * 3) + 5]);
				glm::vec3 pos3(vertices[(i * 3) + 6], vertices[(i * 3) + 7], vertices[(i * 3) + 8]);
				glm::vec2 tex1(textureCoords[i * 2], textureCoords[(i * 2) + 1]);
				glm::vec2 tex2(textureCoords[(i * 2) + 2], textureCoords[(i * 2) + 3]);
				glm::vec2 tex3(textureCoords[(i * 2) + 4], textureCoords[(i * 2) + 5]);
				glm::vec3 tangent = getTangent(pos1, pos2, pos3, tex1, tex2, tex3);
				tangents.push_back(tangent.x);
				tangents.push_back(tangent.y);
				tangents.push_back(tangent.z);
				tangents.push_back(tangent.x);
				tangents.push_back(tangent.y);
				tangents.push_back(tangent.z);
				tangents.push_back(tangent.x);
				tangents.push_back(tangent.y);
				tangents.push_back(tangent.z);
			}
		}

		Material* material = _materials[shape.mesh.material_ids[0]].get();
		_meshes.push_back(std::make_unique<Mesh>(vertices, normals, textureCoords, std::vector<uint32_t>(), tangents, material));
	}
}
